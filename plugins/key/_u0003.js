function (text, cursor_x, cursor_y, show_x, show_y, max_y, max_line, extra_str, max_line_length, is_ctrl_down, is_shift_down, 
        start_selected_x, start_selected_y, end_selected_x, end_selected_y) {
    var ret = {
        text: text,
        copy_str: '',
        cursor_x: cursor_x,
        cursor_y: cursor_y,
        show_x: show_x,
        show_y: show_y
    };
    var lines = text.split('\n');
    var start_x = start_selected_x, 
        start_y = start_selected_y, 
        end_x = end_selected_x, 
        end_y = end_selected_y;
    var need_selected = false;
    if (start_x != end_x || start_y != end_y) {
        need_selected = true;
    }
    if (need_selected) {
        for (var y = 0; y < lines.length; ++y) {
            var line = lines[y];
            if (line.length === 0) {
                ret['copy_str'] += '\n';
                continue;
            }
            for (var x = 0; x < line.length; ++x) {
                var in_same_line = (start_y == end_y);
                if (in_same_line) {
                    if (x >= start_x && x <= end_x && y >= start_y && y <= end_y) {
                        ret['copy_str'] += line[x];
                    }
                } else {
                    if (y > start_y && y < end_y) {
                        ret['copy_str'] += line[x];
                    } else if (y === start_y) {
                        if (x >= start_x) {
                            ret['copy_str'] += line[x];
                        }
                    } else if (y === end_y) {
                        if (x < end_x) {
                            ret['copy_str'] += line[x];
                        }
                    }
                }
            }
            if (y < lines.length - 1) {
                ret['copy_str'] += '\n';
            }
        }
    }
    return ret;
}
