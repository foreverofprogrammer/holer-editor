function (text, cursor_x, cursor_y, show_x, show_y, max_y, max_line, extra_str, max_line_length, is_ctrl_down, is_shift_down, 
        start_selected_x, start_selected_y, end_selected_x, end_selected_y) {
    var ret = {
        text: text,
        cursor_x: cursor_x,
        cursor_y: cursor_y,
        show_x: show_x,
        show_y: show_y
    };
    var lines = text.split('\n');
    if (ret['cursor_x'] > 0) {
        --ret['cursor_x'];
        var line = lines[ret['cursor_y']];
        var deleted = line.substr(0, ret['cursor_x']) + line.substr(ret['cursor_x'] + 1);
        lines[ret['cursor_y']] = deleted;
        ret['text'] = lines.join('\n');
        while (ret['cursor_x'] == ret['show_x'] && ret['show_x'] > 0) {
            --ret['show_x'];
        }
    } else {
        if (ret['cursor_y'] > 0) {
            // 把当前行的内容追加到上一行结尾
            // 并删除当前行
            var prev_line = lines[ret['cursor_y'] - 1];
            var next_line = lines[ret['cursor_y']];
            prev_line += next_line;
            lines[ret['cursor_y'] - 1] = prev_line;
            lines.splice(ret['cursor_y'], 1);
            ret['text'] = lines.join('\n');
            // 如果x为0，y大于0，则要进入上一行末尾处
            --ret['cursor_y'];
            var line_max = max_line_length;
            var line_length = lines[ret['cursor_y']].length;
            while (line_max + ret['show_x'] < line_length) {
                ++ret['show_x'];
            }
            ret['cursor_x'] = line_length - next_line.length;
        }
    }
    return ret;
}
