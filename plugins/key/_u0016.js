function (text, cursor_x, cursor_y, show_x, show_y, max_y, max_line, extra_str, max_line_length, is_ctrl_down, is_shift_down, 
        start_selected_x, start_selected_y, end_selected_x, end_selected_y) {
    var ret = {
        text: text,
        cursor_x: cursor_x,
        cursor_y: cursor_y,
        show_x: show_x,
        show_y: show_y
    };
    var lines = text.split('\n');
    var line = lines[ret['cursor_y']];
    lines[ret['cursor_y']] = line.substr(0, ret['cursor_x']) + extra_str + line.substr(ret['cursor_x'] + 1);
    var str_list = extra_str.split('\n');
    ret['cursor_x'] = str_list[str_list.length - 1].length;
    ret['cursor_y'] += str_list.length - 1;
    ret['text'] = lines.join('\n');
    return ret;
}
