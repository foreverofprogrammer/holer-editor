function (text, cursor_x, cursor_y, show_x, show_y, max_y, max_line, extra_str, max_line_length, is_ctrl_down, is_shift_down, 
        start_selected_x, start_selected_y, end_selected_x, end_selected_y) {
    var ret = {
        text: text,
        cursor_x: cursor_x,
        cursor_y: cursor_y,
        show_x: show_x,
        show_y: show_y
    };
    var lines = text.split('\n');
    var line = lines[ret['cursor_y']];
    var line_length = line.length;
    var left = '', right = '';
    for (var i = 0; i < line_length; ++i) {
        if (i < ret['cursor_x']) {
            left += line[i];
        } else {
            right += line[i];
        }
    }
    lines.splice(ret['cursor_y'], 1);
    lines.splice(ret['cursor_y'], 0, left);
    lines.splice(ret['cursor_y'] + 1, 0, right);
    ret['text'] = lines.join('\n');
    ret['cursor_x'] = 0;
    ++ret['cursor_y'];
    ret['show_x'] = 0;
    while (ret['cursor_y'] + 1 >= max_y + ret['show_y'] && ret['show_y'] < max_y - 1) {
        ++ret['show_y'];
    }
    return ret;
}
