function (text, cursor_x, cursor_y, show_x, show_y, max_y, max_line, extra_str, max_line_length, is_ctrl_down, is_shift_down, 
        start_selected_x, start_selected_y, end_selected_x, end_selected_y) {
    var ret = {
        text: text,
        cursor_x: cursor_x,
        cursor_y: cursor_y,
        show_x: show_x,
        show_y: show_y
    };
    var lines = text.split('\n');
    var text_length = lines[ret['cursor_y']].length;
    if (text_length == 0) {
        if (lines.length > 1 && ret['cursor_y'] < lines.length - 1) {
            lines.splice(ret['cursor_y'], 1);
        }
    } else if (text_length <= ret['cursor_x']) {
        var line1 = lines[ret['cursor_y']];
        var line2 = lines[ret['cursor_y'] + 1];
        lines[ret['cursor_y']] = line1 + line2;
        if (lines.length > ret['cursor_y'] + 1) {
            lines.splice(ret['cursor_y'] + 1, 1);
        }
    } else {
        var line = lines[ret['cursor_y']];
        line = line.substr(0, ret['cursor_x']) + line.substr(ret['cursor_x'] + 1);
        lines[ret['cursor_y']] = line;
    }
    ret['text'] = lines.join('\n');
    return ret;
}
