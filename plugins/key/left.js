function (text, cursor_x, cursor_y, show_x, show_y, max_y, max_line, max_line_length, is_ctrl_down, is_shift_down) {
    var ret = {
        cursor_x: cursor_x,
        cursor_y: cursor_y,
        show_x: show_x,
        show_y: show_y
    };
    var lines = text.split('\n');
    if (ret['cursor_x'] > 0) {
        --ret['cursor_x'];
        if (ret['cursor_x'] == ret['show_x'] && ret['show_x'] > 0) {
            --ret['show_x'];
        }
    } else {
        if (ret['cursor_y'] > 0) {
            if (ret['cursor_y'] == ret['show_y']) {
                --ret['show_y'];
            }
            // 如果x为0，y大于0，则要进入上一行末尾处
            --ret['cursor_y'];
            var line_max = max_line_length;
            var line_length = lines[ret['cursor_y']].length;
            while (line_max + ret['show_x'] <= line_length) {
                ++ret['show_x'];
            }
            ret['cursor_x'] = line_length;
        }
    }
    return ret;
}
