function (text, cursor_x, cursor_y, show_x, show_y, max_y, max_line, max_line_length, is_ctrl_down, is_shift_down) {
    var ret = {
        cursor_x: cursor_x,
        cursor_y: cursor_y,
        show_x: show_x,
        show_y: show_y
    };
    var lines = text.split('\n');
    if (ret['cursor_x'] < lines[ret['cursor_y']].length) {
        ++ret['cursor_x'];
        var max_length = max_line_length;
        while (ret['cursor_x'] > max_length + ret['show_x']) {
            ++ret['show_x'];
        }
    } else {
        if (ret['cursor_y'] < max_line - 1) {
            ret['cursor_x'] = 0;
            ++ret['cursor_y'];
            if (ret['cursor_y'] < lines.length) {
                ret['show_x'] = 0;
                if (ret['cursor_y'] >= max_line) {
                    ++ret['show_y'];
                }
            }
        }
    }
    return ret;
}
