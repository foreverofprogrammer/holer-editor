function (text, cursor_x, cursor_y, show_x, show_y, max_y, max_line, max_line_length, is_ctrl_down, is_shift_down) {
    var ret = {
        cursor_x: cursor_x,
        cursor_y: cursor_y,
        show_x: show_x,
        show_y: show_y
    };
    var lines = text.split('\n');
    if (ret['cursor_y'] < lines.length - 1) {
        ++ret['cursor_y'];
        var line_length = lines[ret['cursor_y']].length;
        if (ret['cursor_x'] > line_length) {
            ret['cursor_x'] = line_length;
        }
        while (ret['cursor_x'] <= ret['show_x'] && ret['show_x'] > 0) {
            --ret['show_x'];
        }
        while (ret['cursor_y'] + 1 > max_y + ret['show_y'] && ret['show_y'] < max_y - 1) {
            ++ret['show_y'];
        }
    }
    return ret;
}
