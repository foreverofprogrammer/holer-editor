function (code) {
    var color_list = [];
    var color = [];
    for (var i in code) {
        var v = code[i];
        if (v == '\n') {
            color_list.push(color);
            color = [];
        } else {
            color.push([255, 0, 0]);
        }
    }
    return color_list;
}